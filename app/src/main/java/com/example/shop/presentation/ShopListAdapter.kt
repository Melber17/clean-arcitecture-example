package com.example.shop.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.example.shop.R
import com.example.shop.domain.ShopItem

class ShopListAdapter : ListAdapter<ShopItem, ShopItemViewHolder>(ShopItemDiffCallback()) {

    var onShopItemLongClickListener: ((ShopItem) -> Unit)? = null
    var onShopItemClickListener: ((ShopItem) -> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopItemViewHolder {
        val layout = when (viewType) {
            ENABLED_NUMBER -> R.layout.item_shop_enabled
            DISABLED_NUMBER -> R.layout.item_shop_disabled
            else -> throw RuntimeException("Unknown type")
        }
        val view =
            LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ShopItemViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ShopItemViewHolder, position: Int) {
        val currentShopItem = getItem(position)
        val status = if (currentShopItem.enabled) {
            "Active"
        } else {
            "Not active"
        }
        viewHolder.titleView.text = "${currentShopItem.name} - $status"
        viewHolder.countView.text = currentShopItem.count.toString()
        viewHolder.view.setOnLongClickListener {
            onShopItemLongClickListener?.invoke(currentShopItem)
            true
        }
        viewHolder.view.setOnClickListener() {
            onShopItemClickListener?.invoke(currentShopItem)
            true
        }
    }

    override fun getItemViewType(position: Int): Int {
        val shopItem = getItem(position)
        return if (shopItem.enabled) {
            ENABLED_NUMBER
        } else {
            DISABLED_NUMBER
        }
    }


    interface OnShopItemLongClickListener {
        fun onShopItemLongClickListener(shopItem: ShopItem)
    }

    companion object {
        const val ENABLED_NUMBER = 1
        const val DISABLED_NUMBER = 0
        const val MAX_POOL_SIZE = 15
    }
}